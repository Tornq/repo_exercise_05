﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{

public class RunBoyRun : MonoBehaviour
{

    public PlayerState playerState;
    private float movementSpeed;

    public void Update()
    {
        switch (playerState)
        {
            case PlayerState.IDLE:
                movementSpeed = 0.45f;
                Debug.Log(message: "Idle boi at 0.5km/h");
                break;

            case PlayerState.NONE:
                movementSpeed = 0;
                Debug.Log(message: "No boi at 0km/h");
                break;

            case PlayerState.RUNNING:
                movementSpeed = 4.295f;
                Debug.Log(message: "Running boi at 4.3km/h");
                break;

            case PlayerState.WALKING:
                movementSpeed = 2.485f;
                Debug.Log(message: "Walking boi at 2.5km/h");
                break;
        
        }
    }


}
}
