﻿using UnityEngine;
using UnityEditor;

namespace CoAHomework
{ 


public enum PlayerState
{
    NONE,
    IDLE,
    WALKING,
    RUNNING,
}
}